import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material';
import { FormControl } from '@angular/forms';

import { DataService } from '../shared/providers/data.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  providers: [DataService],
  encapsulation: ViewEncapsulation.None,
})
export class TableComponent implements OnInit {
  public dataSource: MatTableDataSource<any>;
  public displayedColumns: string[] = [];
  public selection = new SelectionModel<Element>(true, []);
  public listEnvs = ['local', 'dev', 'test', 'int', 'prod'];
  public selectedEvn = new FormControl();

  public iconItems = [
    {text: 'Edit', icon: 'mode_edit'},
    {text: 'Delete', icon: 'delete'},
    {text: 'Add', icon: 'add_circle'},
    {text: 'Change History', icon: 'description'}
  ];

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.initTitleColumns();
    this.dataService.getDataTable().subscribe(
      (data) => {
        this.dataSource = new MatTableDataSource(data);
      });
  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** Simple filtering */
  applyFilter(filterValue: any) {
    if (Array.isArray(filterValue)) {
      this.dataSource.filteredData = filterValue;
    }
    else {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
      this.dataSource.filter = filterValue;
    }
  }

  private initTitleColumns(): void {
    this.displayedColumns = ['select', 'key', 'value', 'environment', 'datetime', 'version', 'context', 'secured', 'action'];
  }

}
