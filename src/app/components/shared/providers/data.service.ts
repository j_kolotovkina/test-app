import { Injectable } from '@angular/core';
import { DataTable } from '../model/data.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class DataService {
  private data = TEST_DATA;

  constructor() {
  }

  getDataTable(): Observable<any> {
    this.data = this.data.map((el) => new DataTable(el));
    return Observable.of(this.data);
  }

}

// Implement static Test Data for table
const TEST_DATA: any[] = [
  {
    key: 'refresh.cycle',
    value: '159ms',
    environment: ['dev', 'test'],
    datetime: '',
    version: '',
    context: ''
  },
  {
    key: 'show.stracktrace',
    value: true,
    environment: ['dev'],
    secured: true,
    datetime: '',
    version: '',
    context: ''
  },
  {
    key: 'log.level',
    value: '',
    environment: ['dev', 'prod'],
    datetime: '',
    version: '',
    context: ''
  },
  {
    key: 'gpv.password',
    value: '',
    environment: ['test', 'prod'],
    secured: true,
    datetime: '',
    version: '',
    context: ''
  },
];
