export class DataTable {
  public key: string;
  public value: any;
  public environment: any[];
  public secured: boolean;
  public datetime: any;
  public version: any;
  public context: any;
  public action: boolean;

  constructor(data) {
    this.key = data.key;
    this.value = data.value;
    this.environment = data.environment;
    this.secured = !!(data.secured);
    this.datetime = data.datetime;
    this.version = data.version;
    this.context = data.context;
    this.action = false;
  }

}
